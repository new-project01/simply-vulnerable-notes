# Simple Notes (Security Demo Application) 🔒🔑

### **Note: Do not edit this project directly, but rather clone it and configure it within your own GitLab instance.**

This application is used for taking simple notes, it has been created to showcase many of GitLab's security features and provide a tutorial around how to use them. Some notable security features covered include:

- Full DevSecOps SDLC
- Static Security Scanners
- Dynamic Security Scanners
- Security Guardrails/Policies
- Vulnerability Resolution
- Vulnerability Management

Get started by viewing the [**Tutorial Documentation**](https://gitlab-de.gitlab.io/tutorials/security-and-governance/devsecops/simply-vulnerable-notes/).

---

## Tutorial / Lessons

You can get started using this project on your own GitLab account with the following lessons:

- Lesson 1: [DevSecOps Overview](https://gitlab-de.gitlab.io/tutorials/security-and-governance/devsecops/simply-vulnerable-notes/getting_started/lesson_1_devsecops_overview/)
- Lesson 2: [Tutorial Prerequisites](https://gitlab-de.gitlab.io/tutorials/security-and-governance/devsecops/simply-vulnerable-notes/getting_started/lesson_2_tutorial_prerequisites/)
- Lesson 3: [Deploying the Demo Application](https://gitlab-de.gitlab.io/tutorials/security-and-governance/devsecops/simply-vulnerable-notes/getting_started/lesson_3_deploying_the_demo_application/)
- Lesson 4: [Setting up and Configuring Security Scanners and Guardrails](https://gitlab-de.gitlab.io/tutorials/security-and-governance/devsecops/simply-vulnerable-notes/getting_started/lesson_4_setting_up_and_configuring_the_security_scanners_and_policies/)
- Lesson 5: [Developer Workflow](https://gitlab-de.gitlab.io/tutorials/security-and-governance/devsecops/simply-vulnerable-notes/getting_started/lesson_5_developer_workflow/)
- Lesson 6: [AppSec Workflow](https://gitlab-de.gitlab.io/tutorials/security-and-governance/devsecops/simply-vulnerable-notes/getting_started/lesson_6_appsec_workflow/)
- Lesson 7: [Policy as code with GitOps](https://gitlab-de.gitlab.io/tutorials/security-and-governance/devsecops/simply-vulnerable-notes/getting_started/lesson_7_policy_as_code_gitops/)
- Lesson 8: [Looking forward](https://gitlab-de.gitlab.io/tutorials/security-and-governance/devsecops/simply-vulnerable-notes/getting_started/lesson_8_looking_forward/)

## Useful Resources

- [GitLab Ultimate Pricing Page](https://about.gitlab.com/pricing/ultimate/)
- [DevOps Solution Resource - DevSecOps](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/devsecops/)
- [What is DevSecOps?](https://about.gitlab.com/topics/devsecops/)
- [Integrating Security into your DevOps workflow](https://about.gitlab.com/solutions/dev-sec-ops/)
- [GitLab Secure Direction Page](https://about.gitlab.com/direction/secure/)
- [GitLab Govern Direction Page](https://about.gitlab.com/direction/govern/)

---

Created and maintained by [Fern](https://gitlab.com/fjdiaz)🌿

- [LinkedIn](https://www.linkedin.com/in/awkwardferny/)
- [Twitter](https://twitter.com/awkwardferny)
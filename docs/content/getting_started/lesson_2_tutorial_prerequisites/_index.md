---
bookCollapseSection: false
weight: 40
---

# Tutorial Prerequisites

In order to get started with GitLab DevSecOps, you will need the following:

**Knowledge**

- Basic knowledge of CI/CD concepts
- Basic knowledge of Kubernetes
- Basic knowledge of the GCP console

**Software/Tools**

- A GitLab account
- A License or Trial for GitLab Ultimate
- Google Cloud SDK
- Kubectl
- Helm
- A Kubernetes Cluster (GKE)
- Ingress-Nginx running on your cluster

You can download and setup all the prerequisites using the links and guides provided below.

## GitLab Account

You may already have a GitLab account, if not you can register here:
https://gitlab.com/users/sign_up

## GitLab Ultimate

In order to get the most out of GitLab DevSecOps, you will require GitLab
Ultimate. If you do not have GitLab Ultimate, you can sign-up for a 30-day
free trial license here: https://about.gitlab.com/free-trial/

## Google Cloud SDK

Before starting, you should download and install the Google Cloud SDK. It will allow us to interact with set our cluster via the command-line. Follow the installation instructions here: https://cloud.google.com/sdk/docs/quickstart

## KubeCtl

The Kubernetes command-line tool, kubectl, allows you to run commands against Kubernetes clusters. We will need it to interact with the cluster we have created via the CLI. Follow the installation instructions here: https://kubernetes.io/docs/tasks/tools/

## Helm

Helm is a package manager for Kubernetes which will allow us to easily install and manage applications within our cluster. We will use it to manage our application as well as it's dependencies. Follow the installation instructions here: https://helm.sh/docs/intro/install/

## Kubernetes Cluster (Manual Creation)

In this example, I will be using GKE, but you can always bring/create a Kubernetes cluster from another service provider. You can get $300 towards a GKE cluster when you first sign-up here: https://cloud.google.com/kubernetes-engine

{{< hint info >}}
**Note:** You can also create a cluster via [Terraform](https://www.terraform.io/) using your Google credentials. If you have knowledge of Terraform and wish to do that instead, skip on over to the [Kubernetes Cluster (Terraform Creation)](./#kubernetes-cluster-terraform-creation) section below.
{{< /hint >}}

Once you have access to the Google cloud console, you can create a cluster as follows:

1. Go to the [Google Cloud Console](https://console.cloud.google.com)

2. Click on the **Menu** Tab

3. Go to the **Kubernetes Engine** Menu in Google Cloud Platform

4. Click on the **Create** button

5. Click on the **Configure** button under **Standard: You manage your cluster**

6. Give the cluster a name

7. Under the **Node Pools** section click on **default-pool**

8. Make sure there are 3 nodes under **Size > Number of nodes**

9. Under the **default-pool** section click on **Nodes**

10. Select the **Series** and a **Machine type**

{{< hint info >}}
**Note:** I selected the **E2** series and the **e2-medium (2 vCPU, 4GB memory)** machine type. This tutorial doesn't require anything more than that and we wanna keep costs low.
{{< /hint >}}

11. Under **Cluster** section click on **Networking**

12. Scroll down and select **Enable Kubernetes Network Policy** as well as **HTTP load balancing** if they aren't already selected

13. Press the **Create** Button

14. Wait for the cluster to render, this will take a bit (10min or so)

15. Click on the rendered cluster

16. Click on the **Connect** button

17. Copy the **Command-line access** command. Then paste the command into your terminal. It should look something like this:

```bash
$ gcloud container clusters get-credentials fern-simple-notes --zone us-central1-c --project fdiaz-02874dfa

Fetching cluster endpoint and auth data.
kubeconfig entry generated for fern-simple-notes.
```

18. Run a simple command to verify the cluster

```bash
$ kubectl cluster-info

Kubernetes control plane is running at https://34.30.156.178
calico-typha is running at https://34.30.156.178/api/v1/namespaces/kube-system/services/calico-typha:calico-typha/proxy
GLBCDefaultBackend is running at https://34.30.156.178/api/v1/namespaces/kube-system/services/default-http-backend:http/proxy
KubeDNS is running at https://34.30.156.178/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy
Metrics-server is running at https://34.30.156.178/api/v1/namespaces/kube-system/services/https:metrics-server:/proxy
 
$ kubectl get nodes

NAME                                               STATUS   ROLES    AGE   VERSION
gke-fern-simple-notes-default-pool-f46c8e7c-mnjt   Ready    <none>   19m   v1.25.8-gke.500
gke-fern-simple-notes-default-pool-f46c8e7c-s7t5   Ready    <none>   19m   v1.25.8-gke.500
gke-fern-simple-notes-default-pool-f46c8e7c-schj   Ready    <none>   19m   v1.25.8-gke.500
```

### Installing an Ingress-Controller

For us to be able to access the application we must install an ingress-controller. While there are many ingress-controllers to chose from, I have decided to use [ingress-nginx](https://github.com/kubernetes/ingress-nginx), since it directly supported by the kubernetes community.

1. Go to the [Google Cloud Console](https://console.cloud.google.com)

2. Click on the **Menu** Tab

3. Go to the **Kubernetes Engine** Menu in Google Cloud Platform

4. Click on the the cluster you previously created

5. Click on the **Connect** button

6. Copy the **Command-line access** command. Then paste the command into your terminal. It should look something like this:

```bash
$ gcloud container clusters get-credentials fern-simple-notes --zone us-central1-c --project fdiaz-02874dfa

Fetching cluster endpoint and auth data.
kubeconfig entry generated for fern-simple-notes.
```

7. Install ingress-nginx using helm, this will take a bit (5min or so)

```bash
$ helm install ingress-nginx ingress-nginx --repo https://kubernetes.github.io/ingress-nginx --namespace ingress-nginx --create-namespace --timeout 17m --set controller.config.enable-real-ip='"true"' --set controller.config.use-forwarded-headers='"true"' --set controller.config.use-proxy-protocol='"true"' --set controller.service.externalTrafficPolicy='"Local"'

NAME: ingress-nginx
LAST DEPLOYED: Tue May 16 12:28:37 2023
NAMESPACE: ingress-nginx
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
The ingress-nginx controller has been installed.
It may take a few minutes for the LoadBalancer IP to be available.
You can watch the status by running 'kubectl --namespace ingress-nginx get services -o wide -w ingress-nginx-controller'
```

8. Verify that the ingress-controller has an **External-IP**

```bash
$ kubectl --namespace ingress-nginx get services -o wide -w ingress-nginx-controller

NAME                       TYPE           CLUSTER-IP    EXTERNAL-IP   PORT(S)                      AGE    SELECTOR
ingress-nginx-controller   LoadBalancer   10.32.1.220   34.29.58.60   80:31242/TCP,443:32026/TCP   105s   app.kubernetes.io/component=controller,app.kubernetes.io/instance=ingress-nginx,app.kubernetes.io/name=ingress-nginx
```

9. **Ping** and **send a request** to verify the **External-IP** is accepting traffic. A **404** is normal since nothing is being hosted on our cluster.

```bash
$ ping 34.29.58.60

PING 34.29.58.60 (34.29.58.60): 56 data bytes
64 bytes from 34.29.58.60: icmp_seq=0 ttl=54 time=54.620 ms
64 bytes from 34.29.58.60: icmp_seq=1 ttl=54 time=52.117 ms
64 bytes from 34.29.58.60: icmp_seq=2 ttl=54 time=55.998 ms
64 bytes from 34.29.58.60: icmp_seq=3 ttl=54 time=295.607 ms

$ curl 34.29.58.60

<html>
<head><title>404 Not Found</title></head>
<body>
<center><h1>404 Not Found</h1></center>
<hr><center>nginx</center>
</body>
</html>
```

## Kubernetes Cluster (Terraform Creation)

If you wish to automate the cluster creation and cleanup process, you can use the provided [terraform scripts](https://gitlab.com/gitlab-de/tutorials/security-and-governance/devsecops/simply-vulnerable-notes/-/tree/main/terraform). Using Terraform will perform the following actions:

- Create the GKE cluster
- Install Ingress-Nginx

### Setup Service Account

In order to get started, you will need a [GCP service account](https://cloud.google.com/docs/authentication#service-accounts) with the following permissions:

- **Compute Network Viewer**: Read-only access to Compute Engine networking resources.
- **Kubernetes Engine Admin**: Full management of Kubernetes Clusters and their Kubernetes API objects.
- **Service Account User**: Run operations as the service account.
- **Service Account Admin**: Create and manage service accounts.

1. Login to [console.cloud.google.com](https://console.cloud.google.com/)

2. Select your [Google Cloud Project](https://cloud.google.com/resource-manager/docs/creating-managing-projects) from the drop down next to the search

3. Navigate to [Service Accounts Page](https://console.cloud.google.com/iam-admin/serviceaccounts)

3. Click on the **Create Service Account** button

4. Add a **Service account name** and **Service account description**

5. Click the **Create and Continue** button

6. Add the roles stated above

7. Click the **Continue** button

8. Click the **Done** button

You should now see a newly created service account in your console screen. Now let's go ahead and create
a key-pair in order to leverage our service account in GitLab.

1. Click on your newly created service account link under the **Email** column

2. Click on **Keys** in the top menu

3. Click the **Add Key** button and select **Create new key**

4. Select **JSON** as the Key type

5. Click the **Create** button

6. Save the JSON to your computer

7. Export the path to the JSON

```bash
$ export GOOGLE_APPLICATION_CREDENTIALS=/path/to/downloaded/credentials.json

$ echo $GOOGLE_APPLICATION_CREDENTIALS
/Users/fern/Desktop/fdiaz-02874dfa-5ba1d84296d8.json
```

### Install and Run Terraform

1. If you have not already done so, [install Terraform](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli)

2. Clone the tutorial repo to your computer

```bash
$ git clone git@gitlab.com:gitlab-de/tutorials/security-and-governance/devsecops/simply-vulnerable-notes.git

Cloning into 'simply-vulnerable-notes'...
remote: Enumerating objects: 3113, done.
remote: Counting objects: 100% (194/194), done.
remote: Compressing objects: 100% (95/95), done.
remote: Total 3113 (delta 71), reused 139 (delta 56), pack-reused 2919
Receiving objects: 100% (3113/3113), 4.97 MiB | 8.49 MiB/s, done.
Resolving deltas: 100% (1675/1675), done.
```

3. Go into the terraform folder

```bash
$ cd simply-vulnerable-notes/terraform
```

4. Run the [terraform init](https://developer.hashicorp.com/terraform/cli/commands/init) command. This prepares the working directory for terraform using the configuration files provided

```bash
$ terraform init

Initializing the backend...

Initializing provider plugins...
- Reusing previous version of hashicorp/google from the dependency lock file
- Reusing previous version of hashicorp/kubernetes from the dependency lock file
- Reusing previous version of hashicorp/helm from the dependency lock file
- Installing hashicorp/google v4.11.0...
- Installed hashicorp/google v4.11.0 (signed by HashiCorp)
- Installing hashicorp/kubernetes v2.20.0...
- Installed hashicorp/kubernetes v2.20.0 (signed by HashiCorp)
- Installing hashicorp/helm v2.9.0...
- Installed hashicorp/helm v2.9.0 (signed by HashiCorp)

Terraform has been successfully initialized!
```

5. Run the [terraform apply](https://developer.hashicorp.com/terraform/cli/commands/apply) command, this will take a bit (17min or so). This command executes the terraform actions to create a cluster and setup the ingress-controller

```bash
$ terraform apply

# Add your GCP Project ID
# https://cloud.google.com/resource-manager/docs/creating-managing-projects
var.gcp_project
  The name of the Google Cloud Project where the cluster is to be provisioned

  Enter a value: fdiaz-02874dfa

# Enter yes to continue
Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

google_container_cluster.primary: Creating...
google_container_cluster.primary: Still creating... [10s elapsed]
...
google_container_cluster.primary: Creation complete after 7m30s [id=projects/fdiaz-02874dfa/locations/us-central1-c/clusters/simple-notes-terraform-gke]
google_container_node_pool.primary_nodes: Creating...
helm_release.ingress-nginx: Creating...
google_container_node_pool.primary_nodes: Still creating... [10s elapsed]
helm_release.ingress-nginx: Still creating... [10s elapsed]
...
google_container_node_pool.primary_nodes: Creation complete after 2m34s [id=projects/fdiaz-02874dfa/locations/us-central1-c/clusters/simple-notes-terraform-gke/nodePools/simple-notes-terraform-gke-node-pool]
helm_release.ingress-nginx: Still creating... [2m40s elapsed]
...
helm_release.ingress-nginx: Creation complete after 17m32s [id=ingress-nginx]

Apply complete! Resources: 3 added, 0 changed, 0 destroyed.
```

### Verify the cluster has been created properly

1. Go to the [Google Cloud Console](https://console.cloud.google.com)

2. Click on the **Menu** Tab

3. Go to the **Kubernetes Engine** Menu in Google Cloud Platform

4. Click on your newly created cluster named **simple-notes-terraform-gke**

5. Click on the **Connect** button

6. Copy the **Command-line access** command. Then paste the command into your terminal

It should look something like this:

```bash
$ gcloud container clusters get-credentials simple-notes-terraform-gke --zone us-central1-c --project fdiaz-02874dfa

Fetching cluster endpoint and auth data.
kubeconfig entry generated for simple-notes-terraform-gke.
```

7. Run a simple command to verify the cluster

```bash
$ kubectl cluster-info

Kubernetes control plane is running at https://35.202.172.194
calico-typha is running at https://35.202.172.194/api/v1/namespaces/kube-system/services/calico-typha:calico-typha/proxy
GLBCDefaultBackend is running at https://35.202.172.194/api/v1/namespaces/kube-system/services/default-http-backend:http/proxy
KubeDNS is running at https://35.202.172.194/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy
Metrics-server is running at https://35.202.172.194/api/v1/namespaces/kube-system/services/https:metrics-server:/proxy
 
$ kubectl get nodes

NAME                                                  STATUS   ROLES    AGE   VERSION
gke-simple-notes-ter-simple-notes-ter-b1475198-fj83   Ready    <none>   25m   v1.24.12-gke.500
gke-simple-notes-ter-simple-notes-ter-b1475198-l3g0   Ready    <none>   25m   v1.24.12-gke.500
gke-simple-notes-ter-simple-notes-ter-b1475198-w32d   Ready    <none>   25m   v1.24.12-gke.500
```

8. Verify that the ingress-controller has an **External-IP**

```bash
$ kubectl --namespace ingress-nginx get services -o wide -w ingress-nginx-controller

NAME                       TYPE           CLUSTER-IP     EXTERNAL-IP       PORT(S)                      AGE   SELECTOR
ingress-nginx-controller   LoadBalancer   10.3.250.172   104.197.219.183   80:30638/TCP,443:30964/TCP   14m   app.kubernetes.io/component=controller,app.kubernetes.io/instance=ingress-nginx,app.kubernetes.io/name=ingress-nginx
```

9. **Ping** and **send a request** to verify the **External-IP** is accepting traffic. A **404** is normal since nothing is being hosted on our cluster.

```bash
$ ping 104.197.219.183

PING 104.197.219.183 (104.197.219.183): 56 data bytes
64 bytes from 104.197.219.183: icmp_seq=0 ttl=54 time=53.791 ms
64 bytes from 104.197.219.183: icmp_seq=1 ttl=54 time=53.916 ms
64 bytes from 104.197.219.183: icmp_seq=2 ttl=54 time=78.329 ms
64 bytes from 104.197.219.183: icmp_seq=3 ttl=54 time=191.930 ms

$ curl 104.197.219.183

<html>
<head><title>404 Not Found</title></head>
<body>
<center><h1>404 Not Found</h1></center>
<hr><center>nginx</center>
</body>
</html>
```

---

Congratulations, you have just met all the prerequisites and created a Kubernetes Cluster with Ingress! Now let's move on to deploying our application using GitLab.

{{< button relref="/lesson_1_devsecops_overview" >}}Previous Lesson{{< /button >}}
{{< button relref="/lesson_3_deploying_the_demo_application" >}}Next Lesson{{< /button >}}